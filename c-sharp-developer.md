Create a dotnet core CLI application that can take in a rectangular site (length and width) and output some buildings metrics based on a site configuration.

- Inputs:
	- width (metres)
	- length (metres)
	- Site configuration
		- num storeys
		- site coverage (pct)
		- Development Type
		- If (Development Type is Apartment or Mixed Use)
			- avg apartment area (m2)
		- If (Development Type is Commercial or Mixed Use)
			- Commercial Mix (pct)
			- Retail Mix (pct)
		- If (Development Type is Mixed Use)
			- Residential Mix (pct)
		- If (Development Type is Subdivision)
			- avg lot size (m2)
		
- Outputs:
	- Site Area (m2)
		- area of site
	- Site Perimeter (m)
		- length of the boundary of the site
	- If (Development Type is Apartment)
		- Building Footprint (m2)
			- site area * site coverage
		- Building GFA (m2)
			- building footprint * num of storeys
		- Number of Apartments
			- Building GFA / avg apartment area
	- If (Development Type is Mixed Use)
		- Building Footprint (m2)
			- site area * site coverage
		- Building GFA (m2)
			- building footprint * num of storeys
		- Residential GFA (m2)
			- building gfa * residential mix
		- Commercial GFA (m2)
			- building gfa * commercial mix
		- Retail GFA (m2)
			- building gfa * retail mix
		- Number of Apartments
			- Residential GFA / avg apartment area
	- If (Development Type is Commercial)
		- Building Footprint (m2)
			- site area * site coverage
		- Building GFA (m2)
			- building footprint * num of storeys
		- Commercial GFA (m2)
			- building gfa * commercial mix
		- Retail GFA (m2)
			- building gfa * retail mix
	- If (Development Type is Subdivision)
		- Number of lots
			- site area * site coverage / avg lot size
	
	
	
- Sample Inputs
```
[
  {
    "width": 100,
    "length": 500,
    "site_config": {
      "num_storeys": 0,
      "site_coverage": 90,
      "development_type": "subdivision",
      "avg_lot_size": 300
    }
  },
  {
    "width": 50,
    "length": 100,
    "site_config": {
      "num_storeys": 3,
      "site_coverage": 70,
      "development_type": "apartment",
      "avg_apt_area": 74
    }
  },
  {
    "width": 250,
    "length": 700,
    "site_config": {
      "num_storeys": 5,
      "site_coverage": 70,
      "development_type": "mixed_use",
      "avg_apt_area": 74,
      "commerical_mix": 20,
      "retail_mix": 70,
      "residential_mix": 10
    }
  },
  {
    "width": 250,
    "length": 700,
    "site_config": {
      "num_storeys": 20,
      "site_coverage": 70,
      "development_type": "commercial",
      "commerical_mix": 20,
      "retail_mix": 70
    }
  },
  {
    "width": 50,
    "length": 100,
    "site_config": {
      "num_storeys": 3,
      "site_coverage": 70,
      "development_type": "apartment"
    }
  },
  {
    "width": 20,
    "length": 30,
    "site_config": {
      "num_storeys": 2,
      "site_coverage": 70,
      "development_type": "apartment",
      "avg_apt_area": 90
    }
  },
  {
    "width": 10,
    "length": 50,
    "site_config": {
      "num_storeys": 0,
      "site_coverage": 90,
      "development_type": "subdivision",
      "avg_lot_size": 450
    }
  },
  {
    "width": 10,
    "length": 50,
    "site_config": {
      "num_storeys": 0,
      "site_coverage": 90,
      "development_type": "subdivision",
      "avg_lot_size": 500
    }
  }
]
```
