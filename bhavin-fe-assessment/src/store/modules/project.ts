/* eslint-disable max-len */
/* eslint-disable no-console */
import { MapboxGeoJSON, Feature } from '../../types';

// Correctly typing GeoJSON Interface, to be used in future unit tests and development convenience
interface ProjectState {
  mapData: MapboxGeoJSON;
  srcMapData: MapboxGeoJSON;
  formValues: any;
  filterData: any;
}

// Initial values
const initialState = {
  srcMapData: null,
  mapData: null,
  formValues: {},
  filterData: {
    title: '',
    address: '',
    categories: [],
    subCategory: '',
    devTypes: [],
    stages: [],
    states: [],
    councils: [],
    ownerships: [],
    street: '',
    suburb: '',
    statuses: [],
    values: [0, 0],
    floorAreas: [0, 0],
    siteAreas: [0, 0],
    storeys: [0, 0],
    units: [0, 0],
  },
};

const getters = {
  getMapData: (state: ProjectState) => state.mapData,
  getFormValues: (state: ProjectState) => state.formValues,
};

const actions = {
  // Wrapping the data in a promise based action, in order to hook up APIs easily in future
  async fetchMapData({ commit }: any): Promise<ProjectState['mapData']> {
    try {
      const mapData = await (await import('../../assets/testBlob.json')).default;

      commit('setMapData', mapData);
      commit('setSrcMapData', mapData);

      return mapData;
    } catch (error) {
      const APIError = 'There was an issue while retriving the list of projects';
      console.error(new Error(`${APIError}\n${error}`));
      throw APIError;
    }
  },
  async processFormValues({ commit, state }: any) {
    // Sorting all numeric values in descending order
    const sortedValues = state.mapData.features
      .map((feature: Feature) => feature.properties.project.Value)
      .sort((a:Number, b: Number) => Number(b) - Number(a));

    const sortedFloorAreas = state.mapData.features
      .map((feature: Feature) => feature.properties.project['Floor Area'])
      .sort((a:Number, b: Number) => Number(b) - Number(a));

    const sortedSiteAreas = state.mapData.features
      .map((feature: Feature) => feature.properties.project['Site Area'])
      .sort((a:Number, b: Number) => Number(b) - Number(a));

    const sortedStoreys = state.mapData.features
      .map((feature: Feature) => feature.properties.project.Storeys)
      .sort((a:Number, b: Number) => Number(b) - Number(a));

    const sortedUnits = state.mapData.features
      .map((feature: Feature) => feature.properties.project.Units)
      .sort((a:Number, b: Number) => Number(b) - Number(a));

    const formValues = {
      // Retrieving the highest possible values for each data fields
      // These will be used for determining the maximum range for corresponding sliders
      maxVal: Number(sortedValues[0]),
      maxFloorArea: Number(sortedFloorAreas[0]),
      maxSiteArea: Number(sortedSiteAreas[0]),
      maxStoreys: Number(sortedStoreys[0]),
      maxUnits: Number(sortedUnits[0]),

      // Using Set to extract unique values
      categories: new Set(
        state.mapData.features.map(
          (feature: Feature) => feature.properties.project.Category,
        ),
      ),
      stages: new Set(
        state.mapData.features.map((feature: Feature) => feature.properties.project.Stage),
      ),
      councils: new Set(
        state.mapData.features.map((feature: Feature) => feature.properties.project.Council),
      ),
      states: new Set(
        state.mapData.features.map((feature: Feature) => feature.properties.project.State),
      ),
      statuses: new Set(
        state.mapData.features.map((feature: Feature) => feature.properties.project.Status),
      ),
      ownerships: new Set(
        state.mapData.features.map(
          (feature: Feature) => feature.properties.project.Ownership,
        ),
      ),
      devTypes: new Set(
        state.mapData.features.map(
          (feature: Feature) => feature.properties.project['Dev. Type'],
        ),
      ),
    };

    // Update state
    commit('setFormValues', formValues);

    return formValues;
  },
  async filterResults(
    { commit, state }: { commit: any; state: ProjectState },
    search: any,
  ): Promise<ProjectState['mapData']> {
    // Copying original source in the beginning
    const filteredData = { ...state.srcMapData };

    // Only performing filter on non default values for optimized performance
    if (search.title !== '') {
      // Title filter
      filteredData.features = filteredData.features.filter((feature: Feature) => {
        const regex = new RegExp(`${search.title}`, 'gi');
        return feature.properties.project.Title.match(regex);
      });
    }

    if (search.address !== '') {
      // Address filter
      filteredData.features = filteredData.features.filter((feature: Feature) => {
        const regex = new RegExp(`${search.address}`, 'gi');
        return feature.properties.project.Address.match(regex);
      });
    }

    if (search.categories.length) {
      // Category filter
      const filtered2DArr = search.categories
        .map((category: String) => filteredData.features.filter((feature: Feature) => {
          const regex = new RegExp(`${category}`, 'gi');
          return feature.properties.project.Category.match(regex);
        }));

      // Merge filtered results into 1 array
      filteredData.features = filtered2DArr
        .reduce((prev: Feature[], cur: Feature[]) => [...prev, ...cur], []);
    }

    if (search.states.length) {
      // States filter
      const filtered2DArr = search.states
        .map((stateName: String) => filteredData.features.filter((feature: Feature) => {
          const regex = new RegExp(`${stateName}`, 'gi');
          return feature.properties.project.State.match(regex);
        }));

      // Merge filtered results into 1 array
      filteredData.features = filtered2DArr
        .reduce((prev: Feature[], cur: Feature[]) => [...prev, ...cur], []);
    }

    if (search.stages.length) {
      // Stages filter
      const filtered2DArr = search.stages
        .map((stage: String) => filteredData.features.filter((feature: Feature) => {
          const regex = new RegExp(`${stage}`, 'gi');
          return feature.properties.project.Stage.match(regex);
        }));

      // Merge filtered results into 1 array
      filteredData.features = filtered2DArr
        .reduce((prev: Feature[], cur: Feature[]) => [...prev, ...cur], []);
    }

    if (search.councils.length) {
      // Stages filter
      const filtered2DArr = search.councils
        .map((council: String) => filteredData.features.filter((feature: Feature) => {
          const regex = new RegExp(`${council}`, 'gi');
          return feature.properties.project.Council.match(regex);
        }));

      // Merge filtered results into 1 array
      filteredData.features = filtered2DArr
        .reduce((prev: Feature[], cur: Feature[]) => [...prev, ...cur], []);
    }

    if (search.street !== '') {
      // Ownership filter
      filteredData.features = filteredData.features.filter((feature: Feature) => {
        const regex = new RegExp(`${search.street}`, 'gi');
        return feature.properties.project.Address.match(regex);
      });
    }
    if (search.suburb !== '') {
      // Ownership filter
      filteredData.features = filteredData.features.filter((feature: Feature) => {
        const regex = new RegExp(`${search.suburb}`, 'gi');
        return feature.properties.project.Suburb.match(regex);
      });
    }

    if (search.ownerships.length) {
      // Stages filter
      const filtered2DArr = search.ownerships
        .map((ownership: String) => filteredData.features.filter((feature: Feature) => {
          const regex = new RegExp(`${ownership}`, 'gi');
          return feature.properties.project.Ownership.match(regex);
        }));

      // Merge filtered results into 1 array
      filteredData.features = filtered2DArr
        .reduce((prev: Feature[], cur: Feature[]) => [...prev, ...cur], []);
    }

    if (search.devTypes.length) {
      // Stages filter
      const filtered2DArr = search.devTypes
        .map((devType: String) => filteredData.features.filter((feature: Feature) => {
          const regex = new RegExp(`${devType}`, 'gi');
          return feature.properties.project['Dev. Type'].match(regex);
        }));

      // Merge filtered results into 1 array
      filteredData.features = filtered2DArr
        .reduce((prev: Feature[], cur: Feature[]) => [...prev, ...cur], []);
    }

    if (search.statuses.length) {
      // Stages filter
      const filtered2DArr = search.statuses
        .map((status: String) => filteredData.features.filter((feature: Feature) => {
          const regex = new RegExp(`${status}`, 'gi');
          return feature.properties.project.Status.match(regex);
        }));

      // Merge filtered results into 1 array
      filteredData.features = filtered2DArr
        .reduce((prev: Feature[], cur: Feature[]) => [...prev, ...cur], []);
    }

    if (Number(search.values[1]) !== 0) {
      // Maxium Value filter
      filteredData.features = filteredData.features.filter(
        (feature: Feature) => Number(feature.properties.project.Value) <= Number(search.values[1]),
      );
    }

    if (Number(search.values[0]) !== 0) {
      // Minimum Value filter
      filteredData.features = filteredData.features.filter(
        (feature: Feature) => Number(feature.properties.project.Value) >= Number(search.values[0]),
      );
    }

    if (Number(search.floorAreas[1]) !== 0) {
      // Maxium Floor Area filter
      filteredData.features = filteredData.features.filter(
        (feature: Feature) => Number(feature.properties.project['Floor Area']) <= Number(search.floorAreas[1]),
      );
    }

    if (Number(search.floorAreas[0]) !== 0) {
      // Minimum Floor Area filter
      filteredData.features = filteredData.features.filter(
        (feature: Feature) => Number(feature.properties.project['Floor Area']) >= Number(search.floorAreas[0]),
      );
    }

    if (Number(search.siteAreas[1]) !== 0) {
      // Maxium Site Area filter
      filteredData.features = filteredData.features.filter(
        (feature: Feature) => Number(feature.properties.project['Site Area']) <= Number(search.siteAreas[1]),
      );
    }

    if (search.siteAreas[0] !== 0) {
      // Minimum Site Area filter
      filteredData.features = filteredData.features.filter(
        (feature: Feature) => Number(feature.properties.project['Site Area']) >= Number(search.siteAreas[0]),
      );
    }

    if (Number(search.units[1]) !== 0) {
      // Maxium Site Area filter
      filteredData.features = filteredData.features.filter(
        (feature: Feature) => Number(feature.properties.project.Units) <= Number(search.units[1]),
      );
    }

    if (search.units[0] !== 0) {
      // Minimum Site Area filter
      filteredData.features = filteredData.features.filter(
        (feature: Feature) => Number(feature.properties.project.Units) >= Number(search.units[0]),
      );
    }

    if (Number(search.storeys[1]) !== 0) {
      // Maxium Site Area filter
      filteredData.features = filteredData.features.filter(
        (feature: Feature) => Number(feature.properties.project.Storeys) <= Number(search.storeys[1]),
      );
    }

    if (search.storeys[0] !== 0) {
      // Minimum Site Area filter
      filteredData.features = filteredData.features.filter(
        (feature: Feature) => Number(feature.properties.project.Storeys) >= Number(search.storeys[0]),
      );
    }

    commit('setMapData', filteredData);

    return filteredData;
  },
};

const mutations = {
  setMapData: (state: ProjectState, mapData: ProjectState['mapData']) => {
    state.mapData = mapData;
  },
  setSrcMapData: (state: ProjectState, mapData: ProjectState['mapData']) => {
    state.srcMapData = mapData;
  },
  setFormValues: (state: ProjectState, formValues: ProjectState['formValues']) => {
    state.formValues = formValues;
  },
};

export default {
  state: initialState,
  getters,
  actions,
  mutations,
};
