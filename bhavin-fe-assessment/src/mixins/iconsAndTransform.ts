export default {
  filters: {
    capitalize(value: String) {
      if (!value) return '';
      return value
        .toLowerCase()
        .split(' ')
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ');
    },
  },
  methods: {
    categoryIcon(category: String) {
      switch (category) {
        case 'HOSPITALITY':
          return 'fa-cocktail';
        case 'COMMERCIAL PREMISES':
          return 'fa-building';
        case 'RETAIL':
          return 'fa-store-alt';
        case 'SOCIAL':
          return 'fa-user-friends';
        case 'PUBLIC BUILDINGS & FACILITIES':
          return 'fa-hospital';
        case 'SPORTS':
          return 'fa-futbol';
        case 'RESIDENTIAL':
          return 'fa-home';
        case 'EDUCATION':
          return 'fa-graduation-cap';
        case 'CIVIL ENGINEERING':
          return 'fa-hard-hat';
        case 'ENTERTAINMENT':
          return 'fa-film';
        default:
          return null;
      }
    },
  },
};
