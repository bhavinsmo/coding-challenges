// Typescript Types
export interface Bin {
  min: number;
  max: number;
  count: number;
  isMedian: boolean;
}

export interface Stats {
  minimumCount: number;
  maximumCount: number;
  maximumValue: number | string;
  minimumValue: number | string;
  median: number | string;
}

export interface BarChart {
  bins: Bin[];
  stats: Stats;
  title: string;
  color: string;
}
export interface LineChart {
  rawCount: number;
  label: string;
  percentage: string;
  color: string;
}
export interface Analytics {
  total: number;
  lineCharts: LineChart[];
  barCharts: BarChart[];
}

export interface Geometry {
  type: string;
  coordinates: number[];
}

export interface Project {
  'Project ID': string;
  Title: string;
  Type: string;
  Address: string;
  Suburb: string;
  State: string;
  Stage: string;
  Category: string;
  SubCategory: string;
  Status: string;
  Council: string;
  'Dev. Type': string;
  'Floor Area': string;
  'Site Area': string;
  Storeys: string;
  Units: string;
  'Commence Date': string;
  'Completion Date': string;
  'Last Updated': string;
  Value: string;
  Ownership: string;
  Description: string;
  Notes: string;
  'Additional Details': string;
  Lat: number;
  Long: number;
}

export interface Properties {
  id: string;
  project: Project;
  color: string;
}

export interface Feature {
  type: string;
  properties: Properties;
  geometry: Geometry;
}

export interface MapboxGeoJSON {
  type: string;
  features: Feature[];
  analytics: Analytics;
}
