
# Front End Assessment
Assessment submission by Bhavin on the challege to create a filtering system in sync with the data on the map

## Front end environment
- Vue 2
- Typescript
- Babel
- Vue Router
- Vuex
- Scss
- AirBNB Eslint Lint Configuration
- Jest Unit Testing
- Nightwatch E2E Testing


## Development

- Install dependencies
  ```
  npm install
  ```

- Run hot reloading server
  ```
  npm run serve
  ```


## Application structure
```sh
Front End Assessment        # Project directory
│
│   config files          # All Environment config files are in root
│
└───src
    │   types.ts          	# Typescript interfaces
    │
    ├───assets              # Contains JSON and Scss files
    ├───components          # Vue Components             
    ├───mixins              # Vue Mixins
    ├───router              # Vue Router
    ├───store             	# Vuex
    └───views            	# Vue Pages

```

## NPM Scripts
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```



**Notes** 

It's been an exciting experience with completing this front end assessment and getting to learn about MapBox GL, some of the best assessments I've came across in my life with a very good creative potential :) 

Made with ❤️ by Bhavin